package ar.fiuba.tdd.tp0;

import java.util.*;
import java.util.function.*;



public class RPNCalculator {

    /** Conceptualmente, el string con la expresion recibida en la eval contiene
        varios elementos (Element) separados por espacios. Un elemento puede ser un 
        operador (binario o n-ario) o bien un numero (representado como una función 
        constante que devuelve siempre el numero en cuestion).
    */

    /** En todos los casos, la interfaz Deque es utilizada para representar un Stack,
        nunca una Queue.
    */

    private interface Element extends Function<Deque<Double>, Double> {}
    
    private Map<String, Element> dictionary;

    public RPNCalculator() {
        dictionary = new HashMap<>();

        BinaryOperator<Double> sum = (a, b) -> a + b;
        registerBinaryOperator("+", sum);
        registerNAryOperator("++", sum);
        
        BinaryOperator<Double> subtract = (a, b) -> a - b;
        registerBinaryOperator("-", subtract);
        registerNAryOperator("--", subtract);
        
        BinaryOperator<Double> multiply = (a, b) -> a * b;
        registerBinaryOperator("*", multiply);
        registerNAryOperator("**", multiply);
        
        BinaryOperator<Double> divide = (a, b) -> a / b;
        registerBinaryOperator("/", divide);
        registerNAryOperator("//", divide);
        
        registerBinaryOperator("MOD",   (a, b) -> a % b);
    }

    public void registerBinaryOperator(String symbol, BinaryOperator<Double> operator) {
        dictionary.put(symbol, operands -> binaryOperator(operands, operator));
    }

    public void registerNAryOperator(String symbol, BinaryOperator<Double> operator) {
        Element binaryOperator = operands -> binaryOperator(operands, operator);
        dictionary.put(symbol, operands -> fold(operands, binaryOperator));
    }

    public double eval(String expression) {
        checkNotNull(expression);
        String[] tokens = expression.split(" ");
        Deque<Double> operands = new LinkedList<>();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            Element element = dictionary.getOrDefault(token, defaultElement(token));
            Double operand = element.apply(operands);
            operands.push(operand);
        }
        checkEquals(1, operands.size());
        return operands.pop();
    }

    private Double binaryOperator(Deque<Double> operands, BinaryOperator<Double> operator) {
        checkMinimumSize(operands, 2);
        Double secondOperand = operands.pop();
        Double firstOperand = operands.pop();
        return operator.apply(firstOperand, secondOperand);
    }

    private Double fold(Deque<Double> operands, Element operator) {
        checkMinimumSize(operands, 1);
        Double result = operands.pop();
        while (!operands.isEmpty()) {
            Deque<Double> currentOperands = new LinkedList<Double>();
            currentOperands.push(result);
            currentOperands.push(operands.pop());
            result = operator.apply(currentOperands);
        }
        return result;
    }

    private Element defaultElement(final String string) { 
        return a -> Double.parseDouble(string);
    }

    private Element exceptionElement() {
        return a -> { throw new IllegalArgumentException(); };
    }

    private void checkNotNull(Object object) {
        checkNotEquals(null, object);
    }

    private void checkMinimumSize(Collection<Double> collection, int minimumSize) {
        for (int i = 0; i < minimumSize; i++) {
            checkNotEquals(i, collection.size());
        }
    }

    private void checkNotEquals(Object invalidKey, Object key) {
        Map<Object, Element> checker = new HashMap<>();
        checker.put(invalidKey, exceptionElement());
        checker.getOrDefault(key, defaultElement("1")).apply(null);
    }

    private void checkEquals(Object validKey, Object key) {
        Map<Object, Element> checker = new HashMap<>();
        checker.put(validKey, defaultElement("1"));
        checker.getOrDefault(key, exceptionElement()).apply(null);
    }

}
